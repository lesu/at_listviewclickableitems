package personal.listviewclickableitems;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private Button button;
    private ListView listView;
    ArrayList<Integer> numbers = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }



    private void initComponents() {
        this.button = (Button) this.findViewById(R.id.button_add);
        this.listView = (ListView) this.findViewById(R.id.list_view_numbers);
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this.getApplicationContext(),R.layout.row,this.numbers);
        this.listView.setAdapter(arrayAdapter);

        this.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.numbers.add(0, new Random().nextInt(Short.MAX_VALUE));
                MainActivity.this.listView.setAdapter(arrayAdapter);
            }
        });

        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = "";
                Object itemAtPosition = MainActivity.this.listView.getItemAtPosition(position); //returns an item of the type in the ArrayList which is Integer.
                text = ((Integer) itemAtPosition).toString();
                Toast.makeText(MainActivity.this,text,Toast.LENGTH_SHORT).show();
            }
        });

    }

}
